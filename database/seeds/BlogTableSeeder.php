<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title'=>'ES BUAH SEGAR,ES BUAH SEGAR,ES BUAH SEGAR',
            'content'=>'SOP BUAH,JUS BUAH,ES BUAH,SOP BUAH,JUS BUAH,ES BUAH,SOP BUAH,JUS BUAH,ES BUAH,SOP BUAH,JUS BUAH,ES BUAH',
            'category'=>'PASTI ENAK!'
        ]);
    }
}
